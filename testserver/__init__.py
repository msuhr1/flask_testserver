from flask import Flask, request

app = Flask(__name__)

@app.route('/')
def index():
    print( request.headers)
    return {'msg': 'This is a test server'}
