# Flask Testserver

Simple server application based on Python Flask. Only return basic JSON response and logs HTTP request to stderr

## Usage
* Clone git repository
* `docker build . -t local/flask_testserver`
* `docker run -it --rm -p 80:80 local/flask_testserver`
* `curl localhost:8080`