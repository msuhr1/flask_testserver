FROM kennethreitz/pipenv

WORKDIR /app

COPY . /app

EXPOSE 80

CMD pipenv run gunicorn --bind 0.0.0.0:80 testserver:app